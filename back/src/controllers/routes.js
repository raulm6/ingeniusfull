const indexControllers = require('../controllers/index.js');

module.exports = function(app, models) {
        // =====================================
        // React Single Page Application =======
        // =====================================
        // Render the SPA
        
        app.get('/', indexControllers.root);
        
        
};
    
