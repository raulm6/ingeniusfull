const connectOptions = require('./secrets.js');

module.exports = function (server) {

    // Socket.io
    var io = require('socket.io')(server);
    // Rethinkdb
    var r = require('rethinkdb');
    // Socket.io changefeed events
    var changefeedSocketEvents = require('./socket-events.js');

    r.connect(connectOptions)
    .then(function(connection) {
        io.on('connection', function (socket) {
    
            // insert new record
            socket.on('record:client:insert', function(record) {
                r.table('TrialTable').insert(record).run(connection);
            });
    
            // update record
            socket.on('record:client:update', function(record) {
                var id = record.id;
                delete record.id;
                r.table('TrialTable').get(id).update(record).run(connection);
            });
    
            // delete record
            socket.on('record:client:delete', function(record) {
                var id = record.id;
                delete record.id;
                r.table('TrialTable').get(id).delete().run(connection);
            });
    
            // emit events for changes to todos
            r.table('TrialTable').changes({ includeInitial: true, squash: true }).run(connection)
            .then(changefeedSocketEvents(socket, 'record'));
        });

    }).error(function(error) {
        console.log('Error connecting to RethinkDB!');
        console.log(error);
    });
}
