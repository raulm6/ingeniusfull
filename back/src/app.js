'use strict';

// ================================
// Initialize Express App =========
// ================================
const express = require('express');
const app = express();

// Load dependencies =============
// ===============================

// Templating engine
const nunjucks = require('nunjucks');
var fs = require('fs');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan       = require('morgan');
var session      = require('express-session');
var env = require('dotenv').load();
var debug = require('debug')('sample:server');
var http = require('http');

// Configuration ==================
// ================================

// Server 
var port = normalizePort('3000'); 
app.set('port', port);

var server = http.createServer(app);

// Database + Sockets
require('./config/database.js')(server);

// Models
//var models = require("./models");
var models = null;

// Log every request to the console
app.use(morgan('dev')); 

// Read cookies (needed for auth)
app.use(cookieParser()); 

// Get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

// Set up Templating Engine
// Through this configuration, Nunjucks will "tell"
// our Express app that it is handling the templates,
// so that when we call the `render` function on a
// response object, it will rely on Nunjucks.
nunjucks.configure('src/build', {
    autoescape: true,
    express: app,
});
app.set('view engine', 'html');

// Set up static public folder
app.use(express.static(__dirname + '/build'));



// Routes ===============================
require('./controllers/routes.js')(app, models); // load our routes and pass in our app and fully configured passport

// Listen ==============================
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

// Utilities ===========================
function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
console.log("Listening on: " + bind);
}




module.exports = app;
    


