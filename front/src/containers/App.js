import React, { Component } from 'react';
import { connect } from 'react-redux';

import Main from '../components/Main/Main.js';

class App extends Component {
  render() {
    return (
      <Main trialtable={this.props.trialtable}/>
    );
  }
}

function mapStateToProps(trialtable) {
  return { trialtable };
}

export default connect(mapStateToProps)(App);


