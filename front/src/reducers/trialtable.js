// reducers/trialtable.js

// trialtable reducer
const trialtable = (state = [], action) => {
    // return index of action's record within state
    const recordIndex = () => {
        return state.findIndex(thisrecord => {
            return thisrecord && thisrecord.id === action.record.id;
        });
    };

    switch(action.type) {
        case 'record:insert':
            // append record at end if not already found in state
            return recordIndex() < 0 ? [...state, action.record].sort(
                sortByProperty('student')) : state;

        case 'record:update':
            // Merge props to update record if matching id
            var index = recordIndex();
            if (index > -1) {
                var updatedrecord = Object.assign({}, state[index], action.record);
                return [...state.slice(0, index), updatedrecord, ...state.slice(index + 1)].sort(
                    sortByProperty('student'));
            }
            else {
                return state;
            }

        case 'record:delete':
            // remove matching record
            var index = recordIndex();
            if (index > -1) {
                return [...state.slice(0, index), ...state.slice(index + 1)].sort(
                    sortByProperty('student'));
            }
            else {
                return state;
            }

        default:
            return state;
    }
};
function sortByProperty(property) {
    
        return function (x, y) {
            let xP = x[property].toLowerCase();
            let yP = y[property].toLowerCase();
            return ((xP === yP) ? 0 : ((xP > yP) ? 1 : -1));
    
        };
    
};

export default trialtable;