// reducers/index.js

import { combineReducers } from 'redux';
import trialtable from './trialtable.js';

const trialApp = combineReducers({ trialtable });

export default trialApp;