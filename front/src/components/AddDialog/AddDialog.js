import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import AddIcon from 'material-ui/svg-icons/content/add';
import { ValidatorForm } from 'react-form-validator-core';
import { TextValidator, DateValidator } from 'react-material-ui-form-validator';
import RaisedButton from 'material-ui/RaisedButton';

// Import socket and connect
import io from 'socket.io-client';
const socket = io.connect('/');

export default class AddDialog extends Component {

  constructor(props) {
    super(props);    
    this.state = {
      open: false,
      newRecord: {},
      defaultDate: new Date()
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { newRecord } = this.state;
    newRecord[event.target.name] = event.target.value;
    this.setState({ newRecord });
    
  }

  handleChangeDate(event, date) {    
    const { newRecord } = this.state;
    newRecord.dateJoined = date;    
    this.setState({ newRecord });
    this.setState({ defaultDate: date});      
  }

  handleSubmit(event) {              
    let formattedRecord =  this.state.newRecord;    
    formattedRecord.dateJoined = this.state.newRecord.dateJoined.toJSON();    
    socket.emit('record:client:insert', formattedRecord);   
    this.setState({
      open: false, 
      newRecord: { 
        student: "", 
        counselor: "", 
        hours: "",
        dateJoined: this.state.newRecord.dateJoined
      }
    });  
  }

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const actions = [      
      <FlatButton
      label="Cancel"          
      onClick={this.handleClose}
    />,
    ];
    let { newRecord } = this.state;    
    return (
      <div className="addDialog">
        <AddIcon onClick={this.handleOpen}/>       
        <Dialog
          title="Add A New Row"
          actions={actions}          
          open={this.state.open}
          onRequestClose={this.handleClose}
          contentStyle={{textAlign: "center"}}
          autoScrollBodyContent={true}
        >
          <ValidatorForm
            onSubmit={this.handleSubmit}            
          >
            <p> Student: </p>
            <TextValidator                
                onChange={this.handleChange}
                name="student"
                type="text"
                validators={['required']}
                errorMessages={['This field is required']}  
                value={newRecord.student}            
            />           
            <p> Counselor: </p>
            <TextValidator               
                onChange={this.handleChange}
                name="counselor"
                type="text"
                validators={['required']}
                errorMessages={['This field is required']}
                value={newRecord.counselor}
            />            
            <p> Hours: </p>
            <TextValidator                
                onChange={this.handleChange}
                name="hours"
                type="number"
                step="0.01"
                max="255"
                min="0"
                validators={['required', 'minNumber:0', 'maxNumber:255', 'matchRegexp:^[0-9]?[0-9]?[0-9](.[0-9][0-9]?)?$']}
                errorMessages={['This field is required', 'No negative hours', 'Max is 255 hours', 'Only 1 or 2 decimals']}
                value={newRecord.hours}
            />            
            <p> Date: </p>
            <DateValidator
              onChange={this.handleChangeDate}
              name="dateJoined"                            
              validators={['required']}
              errorMessages={['This field is required']}              
              value={newRecord.dateJoined}
            />                               
            <br />
            <RaisedButton label="Submit" primary={true} type="submit" />
          </ValidatorForm>
        </Dialog>
      </div>
    );
  }
}