import React, {Component} from 'react';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import "./TrialTableOptions.css";

export default class TrialTableOptions extends Component {
      
    render() {    
        return (      
            <div className="propContainer">
                <h3>Table Properties</h3>
                <TextField
                floatingLabelText="Table Body Height"
                defaultValue={this.props.height}
                onChange={this.props.handleChange}
                />
                <Toggle
                name="fixedHeader"
                label="Fixed Header"
                onToggle={this.props.handleToggle}
                defaultToggled={this.props.fixedHeader}
                />
                <Toggle
                name="fixedFooter"
                label="Fixed Footer"
                onToggle={this.props.handleToggle}
                defaultToggled={this.props.fixedFooter}
                />
                <Toggle
                name="selectable"
                label="Selectable"
                onToggle={this.props.handleToggle}
                defaultToggled={this.props.selectable}
                />
                <Toggle
                name="multiSelectable"
                label="Multi-Selectable"
                onToggle={this.props.handleToggle}
                defaultToggled={this.props.multiSelectable}
                />                
                <h3 className="propToggleHeader">TableBody Properties</h3>
                <Toggle
                name="deselectOnClickaway"
                label="Deselect On Clickaway"
                onToggle={this.props.handleToggle}
                defaultToggled={this.props.deselectOnClickaway}
                />
                <Toggle
                name="stripedRows"
                label="Stripe Rows"
                onToggle={this.props.handleToggle}
                defaultToggled={this.props.stripedRows}
                />
                <Toggle
                name="showRowHover"
                label="Show Row Hover"
                onToggle={this.props.handleToggle}
                defaultToggled={this.props.showRowHover}
                />
            </div>        
    );
  }
}