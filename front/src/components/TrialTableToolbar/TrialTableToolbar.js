import React, {Component} from 'react';
import IconButton from 'material-ui/IconButton';
import SettingsIcon from 'material-ui/svg-icons/action/settings';
import HideIcon from 'material-ui/svg-icons/content/clear';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import AddDialog from '../AddDialog/AddDialog.js';

import "./TrialTableToolbar.css";

export default class TrialTableToolbar extends Component {

  render() {    
    return (
        <Toolbar>                        
            <ToolbarGroup >
            <ToolbarTitle className="toolBarTitle" text="Trial Table" />            
            <ToolbarSeparator />
                <IconButton>
                    <AddDialog/>
                </IconButton> 
            </ToolbarGroup>
            <ToolbarGroup>
                {this.props.showOptions ? 
                    <div onClick={this.props.toggleHideOptions}>
                        <IconButton>
                            <HideIcon/>
                        </IconButton> 
                    </div>
                    :   
                    <div onClick={this.props.toggleShowOptions}>
                        <IconButton>
                            <SettingsIcon/>                        
                        </IconButton> 
                    </div>
                }                    
            </ToolbarGroup>
        </Toolbar> 
    );
  }
}