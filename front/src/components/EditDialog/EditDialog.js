import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { ValidatorForm } from 'react-form-validator-core';
import { TextValidator, DateValidator } from 'react-material-ui-form-validator';
import RaisedButton from 'material-ui/RaisedButton';
import EditIcon from 'react-icons/lib/md/mode-edit';

// Import socket and connect
import io from 'socket.io-client';
const socket = io.connect('/');



export default class EditDialog extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      open: this.props.open,
      newRecord: this.props.row,      
    };  
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const { newRecord } = this.state;
    newRecord[event.target.name] = event.target.value;
    this.setState({ newRecord });
  }

  handleChangeDate(event, date) {    
    let { newRecord } = this.state;
    newRecord.dateJoined = date.toJSON();
    this.setState({ newRecord: newRecord });    
  }

  handleSubmit(event) {                 
    let formattedRecord = this.state.newRecord        
    socket.emit('record:client:update', formattedRecord);        
    this.setState({open: false, newRecord: {}});  
  }

  handleOpen = () => {
    this.setState({open: true, newRecord: this.props.row});    
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const actions = [
      <FlatButton
      label="Cancel"          
      onClick={this.handleClose}
    />,
    ];
    let { newRecord } = this.state;       
    if ( newRecord != null) {     
      newRecord.dateJoined = new Date(newRecord.dateJoined);
      return (
        <div>
          <EditIcon
            size={30}                            
            className="editButton"
            onClick={this.handleOpen}
          />   
          <Dialog
            
            actions={actions} 
            open={this.state.open}
            onRequestClose={this.handleClose}
            contentStyle={{textAlign: "center"}}
            autoScrollBodyContent={true}
            modal={true}
          >
            <ValidatorForm
              onSubmit={this.handleSubmit}
            >
              <p> Student: </p>
              <TextValidator                
                  onChange={this.handleChange}
                  name="student"
                  type="text"
                  validators={['required']}
                  errorMessages={['This field is required']}
                  value={newRecord.student}
              />           
              <p> Counselor: </p>
              <TextValidator                
                  onChange={this.handleChange}
                  name="counselor"
                  type="text"
                  validators={['required']}
                  errorMessages={['This field is required']}
                  value={newRecord.counselor}
              />            
              <p> Hours: </p>
              <TextValidator                
                  onChange={this.handleChange}
                  name="hours"
                  type="number"
                  step="0.01"
                  max="255"
                  min="0"
                  validators={['required', 'minNumber:0', 'maxNumber:255', 'matchRegexp:^[0-9]?[0-9]?[0-9](.[0-9][0-9]?)?$']}
                  errorMessages={['This field is required', 'No negative hours', 'Max is 255 hours', 'Only 1 or 2 decimals']}
                  value={newRecord.hours}
              />            
              <p> Date: </p>
              <DateValidator                
                  onChange={this.handleChangeDate}
                  name="dateJoined"                  
                  validators={['required']}
                  errorMessages={['This field is required']}
                  value={newRecord.dateJoined}
              />                        
              <br />
              <RaisedButton label="Save Edit" primary={true} type="submit" />
            </ValidatorForm>            
          </Dialog>
        </div>
        ); 
      } else {
        return(null);
      }       
  }
}