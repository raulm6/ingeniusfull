import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DelIcon from 'react-icons/lib/md/delete';

// Import socket and connect
import io from 'socket.io-client';
const socket = io.connect('/');




export default class DeleteDialog extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleDelete() {
    socket.emit('record:client:delete', this.props.row);    
    this.handleClose();
  };


  render() {
    const actions = [
      <FlatButton
        label="Yes"
        primary={true}
        keyboardFocused={true}
        onClick={() => this.handleDelete()}
      />,
      <FlatButton
      label="No"          
      onClick={this.handleClose}
    />,
    ];

    return (
      <div className="editDialog">
        <DelIcon       
          size={30}
          className="deleteButton"                                               
          onClick={this.handleOpen}
        />
        <Dialog
          title={`Confirm Delete`}
          actions={actions}          
          open={this.state.open}
          onRequestClose={this.handleClose}
          contentStyle={{textAlign: "center"}}
          autoScrollBodyContent={true}          
          modal={false}
        >
            <p> Are you sure you want to delete the row for {this.props.row.student}? </p>            
        </Dialog>
      </div>
    );
  }
}