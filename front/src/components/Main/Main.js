import React, { Component } from 'react';
import TrialTable from '../TrialTable/TrialTable.js';
import inGeniusLogo from '../../assets/ingenius-logo.png';
import './Main.css';

class Main extends Component {
  render() {
    return (
      <div className="Main">
        <header className="Main-header">          
          <img src={inGeniusLogo} className="Main-logo" alt="InGenius logo" />
          <h1 className="Main-title">Raul's Trial Task Table</h1>
        </header>        
        <TrialTable trialtable={this.props.trialtable}/>
      </div>
    );
  }
}


export default Main;


