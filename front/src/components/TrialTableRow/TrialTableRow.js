import React, {Component} from 'react';
import {
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';

import EditDialog from '../EditDialog/EditDialog.js';
import DeleteDialog from '../DeleteDialog/DeleteDialog.js';

import "./TrialTableRow.css";

import io from 'socket.io-client';
const socket = io.connect('/');

export default class TrialTableRow extends Component {

    handleDelete(record) {
        socket.emit('record:client:delete', record);        
    };

    formatDate(jsonDate){

        let dateObj = new Date(jsonDate);                
        return dateObj.toLocaleDateString();
    }

    render() {
        return (                     
                <TableRow {...this.props}>
                    <TableRowColumn width={60}>                                                                                      
                        <EditDialog 
                            row={this.props.row}
                        />
                    </TableRowColumn>                
                    <TableRowColumn>{this.props.row.student}</TableRowColumn>
                    <TableRowColumn>{this.props.row.counselor}</TableRowColumn>
                    <TableRowColumn>{this.props.row.hours}</TableRowColumn>
                    <TableRowColumn>{this.formatDate(this.props.row.dateJoined)}</TableRowColumn>
                    <TableRowColumn width={60}>
                        <DeleteDialog
                            row={this.props.row}
                        />
                    </TableRowColumn> 
                </TableRow>                                                                         
        );
    }
}