import React, {Component} from 'react';
import {
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import TrialTableToolbar from '../TrialTableToolbar/TrialTableToolbar.js';
import TrialTableOptions from '../TrialTableOptions/TrialTableOptions.js';
import TrialTableRow from '../TrialTableRow/TrialTableRow.js';
import EditDialog from '../EditDialog/EditDialog.js';

import "./TrialTable.css";

export default class TrialTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showOptions: false,
            fixedHeader: true,
            fixedFooter: false,
            stripedRows: false,
            showRowHover: true,
            selectable: false,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: true,            
            height: '500px',
            editDialog: false,
            selectedRow: null,
            selectedRowIndex: null
          };
          this.handleChange = this.handleChange.bind(this);
          this.handleToggle = this.handleToggle.bind(this);          
      }
  

    handleToggle = (event, toggled) => {        
        event.preventDefault();
        this.setState({
        [event.target.name]: toggled,
        });
        
    };

    handleChange = (event) => {
        event.preventDefault();
        this.setState({height: event.target.value});
    };

    hideOptions = (event) => {
        this.setState({showOptions: false});      
        }

    showOptions = (event) => {
        this.setState({showOptions: true});    
    }
   
    render() {        
        return (
        <div className="TrialTableContainer">
            <EditDialog 
                open={this.state.editDialog}
                row={this.props.trialtable[this.state.selectedRow]}
            />            
            <TrialTableToolbar 
            showOptions={this.state.showOptions}
            toggleHideOptions={() => this.hideOptions()}
            toggleShowOptions={() => this.showOptions()}
            />
            <div className = "TableBody">           
                <Table
                height={this.state.height}
                fixedHeader={this.state.fixedHeader}
                fixedFooter={this.state.fixedFooter}
                selectable={this.state.selectable}
                multiSelectable={this.state.multiSelectable}
                >
                    <TableHeader                                            
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                        enableSelectAll={false}
                    >                
                        <TableRow>
                        <TableHeaderColumn width={60} tooltip="Edit Row">Edit</TableHeaderColumn>
                        <TableHeaderColumn tooltip="Student Column">Student</TableHeaderColumn>
                        <TableHeaderColumn tooltip="Counselor Column">Counselor</TableHeaderColumn>
                        <TableHeaderColumn tooltip="Hours Column">Hours</TableHeaderColumn>
                        <TableHeaderColumn tooltip="Date Joined Column">Date Joined</TableHeaderColumn>
                        <TableHeaderColumn width={60} tooltip="Delete Row">Delete</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody                        
                        showRowHover={this.state.showRowHover}
                        stripedRows={this.state.stripedRows}
                    >                        
                        {this.props.trialtable.map( (row, index) => (
                            <TrialTableRow 
                            key={row.id}
                            row={row}
                            {...this.state}                        
                            />
                        ))}
                    </TableBody>
                    <TableFooter
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                        <TableRowColumn width={60}>Edit</TableRowColumn>
                        <TableRowColumn>Student</TableRowColumn>
                        <TableRowColumn>Counselor</TableRowColumn>
                        <TableRowColumn>Hours</TableRowColumn>
                        <TableRowColumn>Date Joined</TableRowColumn>
                        <TableRowColumn width={60}>Delete                  
                        </TableRowColumn>
                        </TableRow>                
                    </TableFooter>
                </Table>
                {this.state.showOptions ?                
                    <TrialTableOptions
                    handleChange={this.handleChange}
                    handleToggle={this.handleToggle}
                    {...this.state}
                    />
                :  
                    null              
                }
            </div>
        </div>
        );
    }
}

