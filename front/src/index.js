import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import store from './stores/trialtable.js';

// Setup our socket events to dispatch
import TodoSocketListeners from './socket-listeners/trialtable.js';

// Needed for Material-UI
import injectTapEventPlugin from 'react-tap-event-plugin';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import registerServiceWorker from './registerServiceWorker';

import App from './containers/App';

import './index.css';

injectTapEventPlugin();
TodoSocketListeners(store);


ReactDOM.render(
<MuiThemeProvider>
    <Provider store={store} >
        <App />
    </Provider>
</MuiThemeProvider>
, document.getElementById('root'));

registerServiceWorker();
