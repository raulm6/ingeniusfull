// actions/index.js
// Socket triggered actions
// Map to socket-events.js on the server
export const newRecord = (record) => {
    return {
        type: 'record:new',
        record: record
    }
}

export const updateRecord = (record) => {
    return {
        type: 'record:update',
        record: record
    }
}

export const deleteRecord = (record) => {
    return {
        type: 'record:delete',
        record: record
    }
}