// socket-listeners/records.js
import io from 'socket.io-client';
const socket = io.connect('/');

export default function(store) {
	socket.on('record:insert', (record) => {
		store.dispatch({
			type: 'record:insert',
			record: record
		});
	});

	socket.on('record:update', function (record) {
		store.dispatch({
			type: 'record:update',
			record: record
		});
	});

	socket.on('record:delete', function (record) {
		store.dispatch({
			type: 'record:delete',
			record: record
		});
	});
}